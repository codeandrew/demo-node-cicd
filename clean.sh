#!/bin/sh

rm -rf .git
rm -rf .gitlab-ci.yml
rm -rf Dockerfile
rm -rf .dockerignore
