#!/bin/sh

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
node -v
npm -v


echo "Installing PM2"
sudo npm install pm2 -g
echo "pm2 start ./bin/www --name <name>"
echo "pm2 list"
